const axios = require('axios')
const TelegramBot = require('node-telegram-bot-api');

const TOKEN = ''

// Create bot
const bot = new TelegramBot(TOKEN, {polling: true});

let users = []

// Matches "/check [whatever]"
bot.onText(/\/check (.+)/, (msg, match) => {
	const chatId = msg.chat.id;
        const username = match[1]; // the captured "whatever"

	 axios.get(`https://twitter.com/users/username_available?username=${username}`)
		.then(response => {
			const { data } = response
			if (data.valid) {
				bot.sendMessage(chatId, 'Username disponível');
			}
			else {
				bot.sendMessage(chatId, 'Username não disponível')
			}
	})      
});

bot.onText(/\/add (.+)/, (msg, match) => {
	const chatId = msg.chat.id
	const username = match[1]

	if (users.filter(item => item.username === username && item.chatId === chatId).length > 0) {
		bot.sendMessage(chatId, 'Username já está na lista...')
	} else {
		users.push({ chatId, username, wasNoticed:false })
		bot.sendMessage(chatId, 'Quando o username estiver disponível, enviarei uma mensagem!');
	}
})

bot.onText(/\/remove (.+)/, (msg, match) => {
	const chatId = msg.chat.id
	const username = match[1]

	users = users.filter(item => item.username !== username && item.chatId === chatId)
	console.log(users)
	bot.sendMessage(chatId, 'Username removido da lista!')
})

setInterval(function () {
	users.forEach(item => { 
		axios.get(`https://twitter.com/users/username_available?username=${item.username}`)
			.then(response => {
				const { data } = response
				if (data.valid && item.wasNoticed === false) {
					bot.sendMessage(item.chatId, '@'+item.username+' disponível agora!');
					item.wasNoticed = true
				} else if (!data.valid && item.wasNoticed === true) {
					bot.sendMessage(item.chatId, '@'+item.username+' não está mais disponível...')
					item.wasNoticed = false
				}
			}) 
		})
	}, 5000);

